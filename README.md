
# Table of Contents

1.  [`= TO BE AMENDED =`](#org51f1157)
2.  [Development of the R-package 'cogmapr'](#org54f1e88)
    1.  [Author](#orgcec2ea7)
    2.  [Description](#orgf1ba8ee)


<a id="org51f1157"></a>

# `= TO BE AMENDED =`


<a id="org54f1e88"></a>

# Development of the R-package 'cogmapr'


<a id="orgcec2ea7"></a>

## Author

Frédéric M. Vanwindekens  

    Walloon agricultural research centre (CRA-W) \\
    Agriculture and natural environment Department (D3) \\
    Farming Systems, Territory and Information Technologies Unit (U11)  
    
    +32 (0)81/62 65 77  --- +32 (0)475/95 21 07  

[f.vanwindekens@cra.wallonie.be](mailto:f.vanwindekens@cra.wallonie.be)

<http://www.cra.wallonie.be>


<a id="orgf1ba8ee"></a>

## Description

The shiny app for visualizing your cognitive maps produced with 'cogmapr'

